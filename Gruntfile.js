module.exports = function(grunt) {
  var pkg = grunt.file.readJSON('package.json');

  grunt.initConfig({
    jade: {
      compile: {
        expand: true,
        cwd: 'dev/jade/public/',
        src: '**/*.jade',
        dest: 'public/',
        ext: '.html',
        options: {
          // min化しない
          pretty: true
        }
      }
    },
    // Compass
    compass: {
      dev: {
        options: {
          sassDir: 'dev/scss',
          cssDir: 'public/assets/css',
          force: true
        }
      }
    },
    // CSS min化
    cssmin: {
      compress: {
        files: {
          'public/assets/css/style.min.css': 'public/assets/css/style.css'
        }
      }
    },
    // JS 構文チェック
    jshint: {
      options: {
        globals: {
          jQuery: true,
          console: true,
          module: true
        },
        unused: true, // 宣言したきり使っていない変数を検出
        // グローバル変数へのアクセスの管理
        browser: true, // ブラウザ用のやつは許可
        devel: true,   // consoleやalertを許可
        expr: true     // x || (x = 1); とかができるようにする
      },
      files: 'dev/js/*.js'
    },
    // JS 結合
    concat: {
      options: {
        // 連結される各ファイル内の間に配置出力する文字列を定義
        separator: ';\n'
      },
      compile: {
        files: {
          'public/assets/js/lib.js': ['dev/js/lib/*.js'],
          'public/assets/js/main.js': ['dev/js/*.js']
        }
      },
      compileDev: {
        files: {
          'public/assets/js/main.js': ['public/assets/js/lib.js', 'public/assets/js/main.js']
        }
      },
      compaileAll: {
        files: {
          'public/assets/js/main.min.js': ['public/assets/js/lib.js', 'public/assets/js/main.min.js']
        }
      }
    },
    // JS min化
    uglify: {
      options: {
        banner: '/*!\n * compile at <%= grunt.template.today("dd-mm-yyyy") %>\n */\n'
      },
      dist: {
        files: {
          'public/assets/js/main.min.js': 'public/assets/js/main.js'
        }
      }
    },
    // JS, CSS gzip化
    compress: {
      css: {
        options: {
          mode: 'gzip'
        },
        files: [
          {
            expand: true,
            src: 'public/assets/css/style.css',
            ext: '.css.gz'
          },
          {
            expand: true,
            src: 'public/assets/css/*.min.css',
            ext: '.min.css.gz'
          }
        ]
      },
      js: {
        options: {
          mode: 'gzip'
        },
        files: [
          {
            expand: true,
            src: 'public/assets/js/main.js',
            ext: '.js.gz'
          },
          {
            expand: true,
            src: 'public/assets/js/*.min.js',
            ext: '.min.js.gz'
          }
        ]
      }
    },
    // ファイル, ディレクトリのコピー
    copy: {
      copyAll: {
        expand: true,
        // コピー元のディレクトリ
        cwd: '<%= dir.bin %>/',
        src: ['**/*.html'],
        // コピー先のディレクトリ
        dest: '<%= dir.release %>/'
      }
    },
    // 不要なファイルを消す
    clean: {
      build: ['public/assets/js/lib.js']
    },

    watch: {
      // jade
      jade: {
        files: ['dev/jade/public/**/*.jade', 'dev/jade/**/*.jade'],
        tasks: 'jade:compile',
        options: {
          nospawn: true
        }
      },
      // CSS
      css: {
        // 監視ファイル
        files: 'dev/scss/*.scss',
        // 実行タスク
        tasks: ['compass:dev', 'cssmin:compress', 'compress:css'],
        options: {
          nospawn: true
        }
      },
      // JS
      js: {
        // 監視ファイル
        files: ['dev/js/*.js', 'dev/js/**/*.js'],
        // 実行タスク
        tasks: ['jshint', 'concat:compile', 'uglify:dist', 'concat:compileDev', 'concat:compaileAll', 'compress:js', 'clean:build'],
        options: {
          nospawn: true
        }
      }
    }
  });


  // loadNpmTasksを変更
  for(var taskName in pkg.devDependencies) {
    if(taskName.substring(0, 6) == 'grunt-') {
      grunt.loadNpmTasks(taskName);
    }
  }

  grunt.registerTask('default', ['watch']);
}
