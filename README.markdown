# ディレクトリ構成 #
```
#!text
dev --- jade // public/以下がpublicにhtmlとして出力されます
 |   |- js   // public/assets/js/にmain.jsとして出力
 |   |   |- lib/ // public/assets/js/lib lib.jsとして出力
 |   |- scss // public/assets/cssに出力
public --- assets
 |           |- js
 |           |- css
 |
Gruntfile.js // gruntで行う ファイル監視, 処理を記述
 |
package.json // このファイルのリストに基づきモジュールをインストールする
```
  
# Grunt環境の構築 #
gruntを使用してjs、cssをコンパイルします。  
※ node.js を使用しているので、先にnode.js, npm環境をインストールしておいて下さい。  
 (特にMacの場合nvmを使うとnpm叩くときにsudoしなくて済むので良いだと思います。)  
[参考]  

1. [CentOSにnvmとNode.jsをインストールする](http://kohkimakimoto.hatenablog.com/entry/2012/11/01/144007)
1. [Mac OS Xにnode.js + expressの環境を作る](http://blog.fumiz.me/2012/02/01/mac-os-x%E3%81%ABnode-js-express%E3%81%AE%E7%92%B0%E5%A2%83%E3%82%92%E4%BD%9C%E3%82%8B/)
  
### ライブラリのインストール ###
このディレクトリで下記コマンドを実行 (pakage.jsonに記載されたリストに基づき必要なライブラリがインストールされます)  
```
#!bash
$ npm install
```

### gruntの実行 ###
このディレクトリで下記コマンドを実行すると、  
Gruntfile.jsの内容に基づきファイル変更を監視しjs、cssをコンパイルします。  
```
#!bash
$ grunt
```
